# PI\_SETUP

When I initialize a new Raspberry Pi I can use this repo to quickly setup all
the basic stuff. And soon some more specialized stuff. It is anoying to start
from scratch again and again.

The setup is based on applying ansible roles to my Raspberry Pi's.

## Setup

To be able to connect to a new Raspberry Pi we first need to enable the SSH
server. This can be done by adding a file named ssh to the boot folder on the
ssd card, using the raspi-config tool or manually enabling the ssh server with:

```
systemctl enable ssh
systemctl start ssh
```

### Create a tunnel to connect to the Raspberry Pi

After that we need to be able to connect to the Pi. Since the Pi is usually
behind a firewall and can be anywhere in the world, it is not straight forward
to connect directly to the pi. In stead I create a connection from the Raspberry
Pi to my VPS with a reverse tunnel to the Pi port 22.

I have many Raspberry Pi computers, so to be able to select any one of them, i
map the hostname to a specific port and use that port is source port for the
tunnel. So a specific hostname is always contacted on a specific forwarded port
on my VPS. I use port 22xx where xx are the first 2 digits in the md5sum of the
hostname. That way every hostname get a single port number which I never have
to remember as long as I remember the hostname. I haven't had any port 
collisions yet.

So all I need as initial setup is starting the ssh service and create an SSH
connection from the Pi to my VPS:

```
  ssh -o port=122 -R 22xx:localhost:22 kristian@ougar.dk
```

## Deploying the Ansbible roles

### Pi init role

To deploy I first need to connect as the pi user. I do this by running the
init.yml playbook using the init shell script. This deploys the pi\_init role,
which configures the system using the raspi-config, fixes access by configuring
ssh options and creating a new user "kristian" and setting the user and root
password to my "standard" default Pi passwords.

Finally the role installs a bunch of standard packages, I always want on my Pi's.

### Pi general setup role

Now I can connect as my normal user, so next playbook is launched seperately. 
This way I never have to deal with connecting as 2 different users in the same
ansible-playbook call.

In general I apply the playbook.yml playbook, which applies the setup, which
I use on all Pi's. 

The playbook.yml playbook can be applied with the run script. This
playbook applies the pi\_general\_setup role, which configures my own user. This
means setting up ssh keys and then checking out my own standard Linux tools,
which I always use. 

The role is applied using the command:

```
./run <hostname>
```

The first time you need to authorize, but the following times, the ssh keys should
work. I have considered moving the keys part to the pi\_init playbook to "fix" this.

In the future i hope that more general setup will be added. For instance I always
want to run my pi tracker, which keeps track of where my Rasperry Pis are.

### More specific setup

If I need specific roles on specific Pi's, my simple plan
is to create a playbook for each hostname, which applies the specific roles
I need. So hopefully I will find time to create the roles, that I need.

On the role todo list I currently have:

  - MariaDB role
  - Nginx role
  - PiHole role
  - Dual network client / hostapd role to use Pi as hotspot and router
  - VPN setup
  - Setting up my Caravan sensor network





 - pi\_general\_setup

With time I will hopefully add a lot of other roles, which can be
