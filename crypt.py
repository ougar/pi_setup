# import the hash algorithm
from passlib.hash import sha512_crypt
import sys

if len(sys.argv)<=1:
  print("You must specify a clear text password to encrypt")
  exit(1)

pw=sys.argv[1]

# generate new salt, and hash a password
hash = sha512_crypt.encrypt(pw)
print(hash)
